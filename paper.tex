\documentclass{article}
\usepackage{biblatex}
\addbibresource{paper.bib}
\usepackage{todonotes}
\usepackage[activate=true]{microtype}
\usepackage{authblk}
\usepackage{longtable}
\usepackage{tabu}
\usepackage{tablefootnote}
\usepackage{multirow}
\usepackage{changepage}

\title{Benchmarking of Hybrid and Post-Quantum Key Exchange and Signature Mechanisms in Network Services}
\author[1]{James Longmore}
\affil[1]{Cogito Group, \emph{james.longmore@cogitogroup.net}}

\begin{document}
    \maketitle
    \begin{center}
        \begin{abstract}
            The timely implementation of quantum-safe key exchange and digital signature algorithms, once standards bodies finalise their selections, will depend upon both the progress of implementing those algorithms into existing standards as well as the practicalities of migrating existing and legacy systems.
            
            In an attempt to quantify tradeoffs that need to be considered during implementation and migration, this paper explores the performance of several quantum-safe key exchange and digital signature algorithms in the context of TLS 1.3 using efforts from the Open Quantum Safe project. The current status of work to support post-quantum and experimental hybrid key exchange specifications is also discussed.
        \end{abstract}
    \end{center}

    \section{Introduction}
    Algorithms known as Post-Quantum (PQ) are based on mathematical operations that are considered hard for quantum computers. This is in contrast to some classical algorithms, such as RSA or Elliptic-Curve Cryptography (ECC), which derive their security from problems that are hard for classical computers but when approached using algorithms made possible by quantum computing, such as Shor's and Grover's Algorithms, become tractable.

    As NIST continues its Post-Quantum Cryptography selection process it is important to consider the implementation of whichever algorithms that might be selected in order to smooth the transition. Crockett et al.\autocite{crockett2019prototyping} examine the challenges around integrating new algorithms into existing standards but it is also important to consider the practicalities of transitioning existing systems to the use of the revised standards. 
    
    The algorithms that have been selected by NIST as PQC Round 2 candidates are generally slower and, when used for key exchange, result in more data being transferred.\autocite{valyukh2017performance} \todo{Cite more examples}
    By introducing barriers such as bandwidth cost and latency these factors may impede the adoption of PQ algorithms despite the clear security benefits that are realised.

    \subsection*{Related Work}
    Since the publication of NIST's Round 2 candidates\autocite{nistround2} in January 2019 there has been significant  effort around the implementation and study of the selected algorithms. \citeauthor{crockett2019prototyping}\autocite{crockett2019prototyping} have explored the implementation of both PQ-only and hybrid mechanisms, \citeauthor{kwiatkowski2019measuring}\autocite{kwiatkowski2019measuring} present real-world measurements of TLS performance, and \citeauthor{valyukh2017performance}\autocite{valyukh2017performance} has studied performance of several PQ algorithms in a standalone context.
    
    Several years prior to any standards processes beginning \citeauthor{bos2015post}\autocite{bos2015post} benchmarked the performance of R-LWE key exchange, ECDH and a hybrid scheme in TLS across a network connection. Their method has been followed closely as it represents a suitable lab analogue to real-world usage and to allow for comparison of results.

    \subsection*{Contributions} 
    This paper studies the performance of PQ algorithms, both by themselves and as part of a hybrid PQ and classical construct, in the context of TLS for the purposes of authentication, key exchange and digital signing. It also includes a brief assessment of efforts to implement PQ algorithms and hybrid protocols at the time of writing.

    \subsection*{Limitations and Future Research}
    \todo{Move to discussion?} The benchmarking performed here relies on the Open Quantum Safe (OQS) project's\footnote{https://openquantumsafe.org/} implementation of algorithms in \emph{liboqs} and their fork of OpenSSL that integrates these into OpenSSL's functionality. As such it is tied to the set of algorithms that have been successfully implemented and does not represent an exhaustive assessment of Round 2 candidate nor PQ algorithms in general.

    As identified by \citeauthor{crockett2019prototyping}\autocite{crockett2019prototyping} there are also several design issues that prevent some PQ algorithms from being used in the TLS 1.3 protocol.
    Future work may include more comprehensive testing of candidate or selected algorithms as the standardisation process progresses or testing of the algorithms identified in this study outside of a lab environment and in a more real-world context, such as that used by \citeauthor{kwiatkowski2019measuring}\autocite{kwiatkowski2019measuring}. 

    \section{Method}
    \subsection{Instrumentation}\todo{Discussion on instrumentation}
    \todo{Copy Bos method, custom client, nginx server}
    Following the method of \citeauthor{bos2015post}\autocite{bos2015post} measurements are taken as the number of full sessions completed per second to an nginx web server serving an xxx KB\todo{How big?} page. 
    To integrate the various algorithms into TLS1.3 the open source implementations from \texttt{liboqs} were used. 
    \subsubsection*{Client}
    The TLS client was a custom built application written in Rust and compiled with \texttt{rustc 1.38.0}. It consists of a variable number of worker threads that simply start a TLS session and request the index page in a loop, and a single main thread responsible for UI updates and consolidating success reports from worker threads.
    \todo{Finish Client function description.}
    \subsubsection*{Server}
    The server in the TLS sessions was a build of nginx x.x.x\todo{Version?} from source. This was modified to link to a build of the OQS fork of OpenSSL and allow the PQ algorithms to be used. 
    The configuration of the server was modified to enable HTTPS, run several worker processes and disable HTTPS session caching.
    This was running on a Fedora x\todo{Version} virtual machine assigned 4 CPU cores and x\todo{RAM?} GB of RAM.
    \subsection{Timing}
    To allow for high precision timing, beyond that which is provided by OS APIs, timing is performed at a low level using the \texttt{rdtsc} x86\_64 assembly instruction. This instruction returns the contents of a 64-bit Time Stamp Counter (TSC) register that counts the number of cycles since the processor was reset. By counting the number of ticks that occur over a duration and then dividing by the processor's clockspeed timing with nanosecond precision is possible.

    On modern processors that can vary their clock speed for efficiency or performance purposes this is unreliable as the number of ticks accumulated over a given duration may not be consistent. More recent processors include an `invariant TSC' that ticks at the nominal frequency of the CPU regardless of the actual operating clockspeed and serves as a reliable wall clock measurement.

    For this research, timing is done on a system using an Intel Core i7-8700 CPU, which has an invariant TSC feature.

    \subsection{Algorithm Selection}
    The algorithms that were tested only represent the subset of the set of NIST PQC Round 2 candidates that has been implemented in \emph{liboqs} and that have been successfully implemented in OpenSSL as a TLS KEM. A summary of the individual algorithms selected is in Table~\ref{tab:AlgSummary}. \todo{Fix footnotes and reference the shit out of this}

    \tabulinesep=0.4em
    \begin{center}
        \begin{longtabu} to 1.1\textwidth {|X[l] X[c] X[c] X[c] X[c,m]|X[c] X[c]|}
        \caption{Summary of tested KEM algorithms}\label{tab:AlgSummary}\\
        \multicolumn{5}{c}{} & \multicolumn{2}{c}{Key Size} \\
        \multicolumn{5}{c}{} & \multicolumn{2}{c}{(Bytes)} \\ \hline 
        Algorithm & Type & Problem & Variant & Security \linebreak Level & Secret & Public \\ \hline
        \multirow{2}{*}{ECC} & \multirow{2}{\hsize}{Elliptic Curve} & \multirow{2}{\hsize}{Discrete Logarithm} &
        P256 & L1 & & \\
        & & & P384 & L3 & & \\ \hline
        \multirow{4}{*}{BIKE 1} & \multirow{4}{\hsize}{Code} & \multirow{4}{*}{QC-MDPC\tablefootnote{Quasi-Cyclic Medium Density Parity Check}} &
        L1CPA & ?? & & \\
        & & & L1FO & ?? & & \\
        & & & L3CPA & ?? & & \\
        & & & L3FO & ?? & & \\ \hline
        \multirow{6}{*}{FrodoKEM} & \multirow{6}{\hsize}{Lattice} & \multirow{6}{\hsize}{LWE\tablefootnote{Learning With Errors}} &
        640AES & L1 & & \\
        & & & 640SHAKE & L1 & & \\
        & & & 976AES & L3 & & \\
        & & & 976SHAKE & L3 & & \\
        & & & 1344AES & L5 & & \\ 
        & & & 1344SHAKE & L5 & & \\ \hline
        \multirow{3}{\hsize}{Kyber\autocite{avanzi2017crystals}} & \multirow{3}{\hsize}{Lattice} & \multirow{3}{*}{Module-LWE} &          
        512 & L1 & 1632 & 800 \\ 
        & & & 768 & L3 & 2400 & 1184 \\
        & & & 1024 & L5 & 3168 & 1568 \\ \hline
        \multirow{2}{\hsize}{NewHope} & \multirow{2}{\hsize}{Lattice} & \multirow{2}{*}{Ring-LWE} &          
        512 & ?? & & \\ 
        & & & 1024 & ?? & & \\ \hline
        \multirow{4}{\hsize}{NTRU} & \multirow{4}{\hsize}{Lattice} & \multirow{4}{\hsize}{Shortest Vector Problem} & 
        HPS2048509 & ?? & & \\ 
        & & & HPS2048677 & ?? & & \\
        & & & HPS4096821 & ?? & & \\
        & & & HRSS701 & ?? & & \\ \hline
        \multirow{3}{\hsize}{Saber} & \multirow{3}{\hsize}{Lattice} & \multirow{3}{\hsize}{Module - LWR\tablefootnote{Learning With Rounding}} & 
        Lightsaber & L1 & & \\ 
        & & & Saber & L3 & & \\
        & & & Firesaber & L5 & & \\ \hline
        \multirow{4}{\hsize}{SIDH} & \multirow{4}{\hsize}{Elliptic Curve} & \multirow{4}{\hsize}{Supersingular Isogeny} & 
        P434 & ?? & & \\ 
        & & & P503 & ?? & & \\
        & & & P610 & ?? & & \\
        & & & P751 & ?? & & \\ \hline
        \multirow{4}{\hsize}{SIKE} & \multirow{4}{\hsize}{Elliptic Curve} & \multirow{4}{\hsize}{Supersingular Isogeny} & 
        P434 & ?? & & \\ 
        & & & P503 & ?? & & \\
        & & & P610 & ?? & & \\
        & & & P751 & ?? & & \\ \hline
        \end{longtabu}
    \end{center}


    \newpage
    \printbibliography
\end{document}