use tui::backend::Backend;
use tui::Frame;
use tui::layout::Rect;
use tui::style::{Style, Modifier};
use tui::widgets::{Block, Paragraph, Text, Widget, Borders};

pub fn draw_header<B: Backend>(mut f: &mut Frame<B>, chunk: Vec<Rect>) {
    let banner = r#"    ___  ___ ____ ___  ___ ___ _ 
   / _ \/ _ `(_-</ _ \/ _ `/  ' \
  / .__/\_, /___/ .__/\_,_/_/_/_/
 /_/     /_/   /_/               "#;

    let banner = [ Text::raw(banner) ];

    let help1 = [ Text::styled("q: Quit\nSpace: Start/Stop\nTab: Snapshot datapoint", Style::default().modifier(Modifier::BOLD)) ];

    Paragraph::new(banner.iter())
        .block(
            Block::default()
                .borders(Borders::NONE)
        )
        .render(&mut f, chunk[0]);
    
    //TODO: Split into 3 lists when needed
    Paragraph::new(help1.iter())
        .block(
            Block::default()
            .borders(Borders::ALL)
            .title("Help")
        )
        .render(&mut f, chunk[1]);

}

pub fn draw_logs<B: Backend>(mut f: &mut Frame<B>, chunk: Rect) {
    tui_logger::TuiLoggerWidget::default()
        .block(
                Block::default()
                .borders(Borders::ALL)
                .title("Logs")
        )
        .render(&mut f, chunk);
}