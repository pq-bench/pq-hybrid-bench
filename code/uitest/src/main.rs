pub mod ui;

#[macro_use]
extern crate log;

use log::LevelFilter;
use std::io;
use std::io::Read;
use std::thread;
use std::time::Duration;
use termion::raw::IntoRawMode;
use tui::backend::{TermionBackend};
use tui::Terminal;
use tui::layout::{Constraint, Direction, Layout};
use tui::widgets::{Block, Borders, Widget};
use tui_logger;

use clap::{Arg, App, ArgMatches};

const KEY_Q: u8 = 113;
const KEY_SPACE: u8 = 32;
const KEY_TAB: u8 = 9;

struct UiFlags {
    redraw: bool,
    quit: bool,
    running: bool,
}

fn main() -> io::Result<()> {

    let args = parse_args().get_matches(); //Putting this first in case it needs to print errors or help before we take control of the console

    let stdout = io::stdout().into_raw_mode()?;
    let mut stdin = termion::async_stdin().bytes();

    let backend = TermionBackend::new(stdout);
    let mut term = Terminal::new(backend)?;

    let mut state = UiFlags { redraw: true, quit: false, running: false };

    term.clear()?;

    tui_logger::init_logger(LevelFilter::Trace).unwrap();
    tui_logger::set_level_for_target("uitest", LevelFilter::Debug);

    debug!("Logger initialised");
    debug!("Args: {:?}", arg_pretty_print(&args));

    // DEBUG: Starting with : <Parameter list>

    loop {
        let b = stdin.next();
        match b {
            Some(Ok(s)) => {
                debug!( "Handling key {}", s);
                handle_key(s, &mut state);
            },
            _ => {}
        }

        if state.redraw {
            draw_ui()?;
            state.redraw = false;
        }

        if state.quit {
            break;
        }
        thread::sleep(Duration::from_millis(20));
    }

    draw_ui()?; //Final draw before exit. Log messages or whatever.
    
    Ok(())
}

fn arg_pretty_print<'a>(args: &ArgMatches) -> String {
   let s = format!{"Algorithm: {}, Host: {}, Port: {}, Num Threads: {}, Debug: {}, Paused: {}",
    args.value_of("alg").unwrap(), 
    args.value_of("host").unwrap(), 
    args.value_of("port").unwrap(),
    args.value_of("num_threads").unwrap(),
    args.is_present("verbose"),
    args.is_present("paused")
   };
   s
}
fn parse_args<'a,'b>() -> App<'a,'b> {
    App::new("pqspam")
        .version("0.1")
        .author("James Longmore <jlongmore@cogitogroup.net>")
        .about("A very specific HTTPS client, for benchmarking KEX algorithms.")
        .arg(Arg::with_name("alg")
            .index(1)
            .required(true)
            .help("Key exchange suite specification"))
        .arg(Arg::with_name("host")
            .short("h")
            .long("host")
            .default_value("localhost")
            .help("Host to connect to"))
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .default_value("443")
            .help("Port to connect on"))
        .arg(Arg::with_name("num_threads")
            .short("n")
            .long("num-threads")
            .default_value("1")
            .help("Number of threads to run"))
        .arg(Arg::with_name("verbose")
            .short("v")
            .help("Be verbose"))
        .arg(Arg::with_name("paused")
            .help("Don't automatically start benchmarking"))
}

fn handle_key(k: u8, state: &mut UiFlags ) {
    match k {
        KEY_Q => state.quit = true,
        KEY_SPACE => debug!("Not implemented - Start/Stop"),
        KEY_TAB => debug!("Not implemented - Snapshot"),
        _ => debug!("No handler for {}", k)
    }
    state.redraw = true;
}

fn draw_ui() -> io::Result<()>{

    let stdout = io::stdout().into_raw_mode()?;

    let backend = TermionBackend::new(stdout);
    let mut term = Terminal::new(backend)?;

    term.draw(|mut f| {
        let sz = f.size();

        let layout = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(5), Constraint::Percentage(90)].as_ref())
            .split(sz);

        let header = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(60), Constraint::Percentage(40)].as_ref())
            .split(layout[0]);
        
        let main_pane = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(50),Constraint::Percentage(50)].as_ref())
            .split(layout[1]);
        
        let data_pane = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Percentage(60),Constraint::Percentage(40)].as_ref())
            .split(main_pane[0]);

        ui::draw_header(&mut f, header);

        ui::draw_logs(&mut f, main_pane[1]);
            
        Block::default()
            .borders(Borders::ALL)
            .title("Chart")
            .render(&mut f, data_pane[0]);

        Block::default()
            .borders(Borders::ALL)
            .title("Stats")
            .render(&mut f, data_pane[1]);
    })?;

    Ok(())
}
