use core::arch::x86_64::_rdtsc;
use std::thread;
use std::time::Duration;

pub fn get_rdtsc_ticks() -> u64 {
    unsafe {
        _rdtsc()
    }
}

pub fn get_ticks_per_second() -> u64 {
    unsafe {
	let start = _rdtsc();
	thread::sleep(Duration::from_millis(100));
	let duration = _rdtsc() - start;
	duration * 10
    }
}
