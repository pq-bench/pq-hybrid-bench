#[macro_use]
extern crate log;
extern crate statrs;
#[macro_use]
extern crate clap;

pub mod ui;
pub mod bench;
pub mod timing;

use log::LevelFilter;
use std::io;
use std::io::{Write, Read};
use std::thread;
use std::time::Duration;
use termion::raw::IntoRawMode;
use tui::backend::{TermionBackend};
use tui::Terminal;
use tui::layout::{Constraint, Direction, Layout};
use tui::widgets::{Block, Borders, Widget};
use tui_logger;
use std::sync::{Arc, Mutex, RwLock};
use std::str::FromStr;
use std::fs;

use std::collections::vec_deque::VecDeque;

use clap::{Arg, App};

use statrs::statistics::Statistics;

const KEY_Q: u8 = 113;
const KEY_SPACE: u8 = 32;
const KEY_TAB: u8 = 9;
const KEY_R: u8 = 114;
const KEY_MINUS: u8 = 45;
const KEY_PLUS: u8 = 43;

const NUM_SAMPLES: usize = 30; // Number of previous samples to keep for our chart.

// Stores a snapshot of calculated stats, in requests/sec, with total, mean and std_dev.
#[derive(Debug)]
pub struct Stats{
    thread_stats: Vec<f64>,
    total: f64,
    mean: f64,
    std_dev: f64
}

pub struct AppState {
    redraw: bool,
    quit: bool,
    running: bool,
    host: String,
    port: u16,
    algs: Vec<String>,
    init_threads: u8,
    num_threads: u8,
    stats: Vec<RwLock<ThreadStats>>,
    samples: RingBuf<f64>,
    last_max: f64,
    out_file: String,
    tps: u64,
    start_time: u64,
}

impl AppState {
    fn calc_stats(&mut self) -> Stats {

        let now = timing::get_rdtsc_ticks();

        let values = self.stats.iter().map(|s| {
            let lock = s.read().unwrap();
            let elapsed = now - lock.start_time;
            lock.requests as f64 / (elapsed as f64 / self.tps as f64)
        })
        .collect::<Vec<f64>>();

        let sum = values.iter().sum();
        let mean = sum / (values.len() as f64);
        let std_dev = values.clone().std_dev();

        self.samples.push(sum);

        Stats {
            thread_stats: values,
            total: sum,
            mean: mean,
            std_dev: std_dev
        }
    }
}

#[derive(Debug)]
pub struct ThreadStats {
    start_time: u64, //Start time, measured in _rdtsc ticks
    requests: u64, // Lifteim number of successful requests.
}

impl Default for ThreadStats {
    fn default() -> Self {
        Self{start_time: timing::get_rdtsc_ticks(), requests: 0}
    }
}

#[derive(Debug)]
pub struct RingBuf<T> {
    deque: VecDeque<T>,
    size: usize,
}

impl<T> Default for RingBuf<T> {
    fn default() -> Self {
        Self { deque: VecDeque::<T>::with_capacity(NUM_SAMPLES), size: NUM_SAMPLES }
    }
}
impl<T: Copy> RingBuf<T> {
    // Pushes onto the back of the buffer, knocking off the front if we're out of room
    fn push(&mut self, item: T) {
        if self.deque.len() >= self.size {
            self.deque.pop_front();
        }
        self.deque.push_back(item);
    }    

    // Returns the contained deque: itesm along with its position in the queue
    fn get_datapoints(&self) -> Vec<T> {

        //Cloning so we dont get data changed on us
        self.deque.iter().copied().collect()
    }
}


fn main() -> io::Result<()> {

    let args = parse_args().get_matches(); //Put this first in case it needs to print errors or help before we take control of the console

    let mut stdin = termion::async_stdin().bytes();

    let mut state = AppState{ 
        redraw: true, 
        quit: false, 
        running: false,
        algs: vec![],
        host: value_t!(args, "host", String).unwrap(),
        port: value_t!(args, "port", u16).unwrap(),
        init_threads: value_t!(args, "num_threads", u8).unwrap(),
        num_threads: value_t!(args, "num_threads", u8).unwrap(),
        stats: vec![],
        samples: RingBuf::default(),
        last_max: 0.0,
        out_file: value_t!(args, "out_file", String).unwrap(),
        tps: timing::get_ticks_per_second(),
        start_time: 0,
    };

    if args.is_present("file") {
        state.algs = algs_from_file(args.value_of("file").unwrap());
    } else {
        state.algs = vec![ value_t!(args, "alg", String).unwrap() ];
    }
    let state = Mutex::new(state);
    let state = Arc::new(state);

    let mut workers = Vec::new();

    println!("{}", termion::clear::All);

    tui_logger::init_logger(LevelFilter::Trace).unwrap();
    if args.is_present("verbose") {
        tui_logger::set_level_for_target("pqspam", LevelFilter::Debug);
    } else {
        tui_logger::set_level_for_target("pqspam", LevelFilter::Info);
    }

    debug!("Logger initialised");
    debug!("{:?}",  args);
    debug!("Ticks per sec: {}", timing::get_ticks_per_second());

    if !args.is_present("paused") {
        workers = start_workers(&state);
    }

    loop {
        let b = stdin.next();
        match b {
            Some(Ok(s)) => {
                debug!( "Handling key {}", s);
                handle_key(s, &state);
            },
            _ => {}
        }

        let mut s = state.lock().unwrap();
        
        if s.redraw {
            println!("{}", termion::clear::All );
            s.redraw = false; //Commented out for now. On demand redraw causing some Ui issues
        } 

        if s.quit {
            debug!(target: "pqspam", "Stats: {:?}", s.calc_stats());
            break;
        }

        let running = s.running;
        let start = s.start_time;
        let tps = s.tps;

        drop(s);
        
        draw_ui(&state)?;
        if args.is_present("ramp") && running {
            ramp_threads(&state);
        }

        if args.is_present("auto") && running {
            if (timing::get_rdtsc_ticks() - start) >= value_t!(args, "auto", u64).unwrap() * tps {
                take_snapshot(&state)?;
                next_algorithm(&state);
                thread::sleep(Duration::from_millis(500)); //Give workers a chance to die
                start_workers(&state);
            }
        }

        thread::sleep(Duration::from_millis(1000/60));
    }

    // Wrap up our workers
    for t in workers {
        t.join().unwrap();
    }

    println!("{}", termion::clear::All);
    draw_ui(&state)?; //Final, fresh draw before exit. Log messages from threads or whatever.
    Ok(())
}

// Read the stats. If the difference between our last measurement and the one before is greater than the ramp threshold
// increase the number of running threads
fn ramp_threads(state: &Arc<Mutex<AppState>>) {
    const RAMP_THRESHOLD: f64 = 3.0;
    const RAMP_NUM: u8 = 1;

    let mut s = state.lock().unwrap();

    let samples = s.samples.get_datapoints();
    let max = s.samples.get_datapoints().max();

    if samples.len() < 2 {
        return;
    }

    if max >= (s.last_max + RAMP_THRESHOLD) {
        s.num_threads += RAMP_NUM;
        s.last_max = max;
        drop(s);
        start_workers(&state);
    }
}

fn algs_from_file(file: &str) -> Vec<String> {
    // Just read in the file as lines. Each line is an algorithm
    let mut algs: Vec<String> = fs::read_to_string(file).unwrap().split("\n").map(|x| String::from_str(x).unwrap()).collect();    
    if algs.ends_with([String::new()].as_ref()) {
        algs.pop();
    }
    algs
}

fn parse_args<'a,'b>() -> App<'a,'b> {
    App::new("pqspam")
        .version("0.1")
        .author("James Longmore <jlongmore@cogitogroup.net>")
        .about("A very specific HTTPS client, for benchmarking KEX algorithms.")
        .arg(Arg::with_name("alg")
            .index(1)
            .required_unless("file")
            .help("Key exchange suite specification"))
        .arg(Arg::with_name("file")
            .short("f")
            .long("file")
            .takes_value(true)
            .help("File with a list of algorithms to run"))
        .arg(Arg::with_name("host")
            .short("h")
            .long("host")
            .default_value("localhost")
            .help("Host to connect to"))
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .default_value("443")
            .help("Port to connect on"))
        .arg(Arg::with_name("num_threads")
            .short("n")
            .long("num-threads")
            .default_value("1")
            .help("Number of threads to run"))
        .arg(Arg::with_name("verbose")
            .short("v")
            .help("Be verbose"))
        .arg(Arg::with_name("paused")
            .long("paused")
            .help("Don't automatically start benchmarking"))
        .arg(Arg::with_name("out_file")
            .short("o")
            .long("out")
            .default_value("results.csv")
            .help("File to output data snapshots to"))
        .arg(Arg::with_name("ramp")
            .long("ramp")
            .help("Ramp the number of threads up automatically"))
        .arg(Arg::with_name("auto")
            .long("auto")
            .takes_value(true)
            .help("Automatic mode - take measurment every n seconds"))
}

fn handle_key(k: u8, state: &Arc<Mutex<AppState>>) {
    let mut s = state.lock().unwrap();
    s.redraw = true;

    match k {
        KEY_Q => {
            info!("Quitting");
            s.quit = true;
        },
        KEY_SPACE => {
            if s.running {
                info!("Stopping"); 
                s.running = false;
                s.stats = vec![];
                s.num_threads = s.init_threads;
            } else {
                s.stats = vec![]; //Reset the stats vec
                drop(s);
                start_workers(state);
            }
        },
        KEY_TAB => {
            drop(s);
            take_snapshot(&state).unwrap();
            next_algorithm(&state);
            // start_workers(state);
        },
        KEY_R => {
            print!("{}", termion::clear::All);
        },
        KEY_MINUS => {
            s.num_threads -= 1;
            info!(target:"pqsqam", "Decreasing thread count to {}", s.num_threads);
        },
        KEY_PLUS => {
            s.num_threads += 1;
            info!(target:"pqspam", "Increasing thread count to {}", s.num_threads);
            drop(s);
            start_workers(state);
        }
        _ => {debug!("No handler for {}", k); }
    }
}

fn take_snapshot(state: &Arc<Mutex<AppState>>) -> io::Result<()> {
    let mut s = state.lock().unwrap();
    let snap = s.calc_stats();
    let f = &s.out_file;

    // Open file in append, and write

    let mut f = fs::OpenOptions::new().create(true).append(true).open(f).unwrap();
    writeln!(&mut f, "{},{},{},{},{}", s.algs[0], s.num_threads, snap.total, snap.mean, snap.std_dev)?;
    s.last_max = 0.0;
    drop(s);
    info!(target:"pqspam", "Snapshot taken");
    Ok(())
}

fn next_algorithm(state: &Arc<Mutex<AppState>>) {
    let mut s = state.lock().unwrap();
    s.running = false;
    if s.algs.len() == 1 {
        s.quit = true;
        return;
    }
    drop(s);
    thread::sleep(Duration::from_millis(100)); // Give threads time to finish before recycling.

    s = state.lock().unwrap();
    s.algs.remove(0);
    debug!(target: "pqspam", "Next Alg: {:?}", s.algs);
    s.stats = vec![];
    s.samples = RingBuf::default() ;
    print!("{}", termion::clear::All);
    s.num_threads = s.init_threads;
    drop(s);
    info!(target:"pqspam", "Press SPACE to start");
}

fn draw_ui(state: &Arc<Mutex<AppState>>) -> io::Result<()>{

    let stdout = io::stdout().into_raw_mode()?;

    let backend = TermionBackend::new(stdout);
    let mut term = Terminal::new(backend)?;
    term.hide_cursor().unwrap();

    term.draw(|mut f| {
        let sz = f.size();

        let layout = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Min(5), Constraint::Percentage(90)].as_ref())
            .split(sz);

        let header = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(60), Constraint::Percentage(40)].as_ref())
            .split(layout[0]);
        
        let main_pane = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Percentage(50),Constraint::Percentage(50)].as_ref())
            .split(layout[1]);
        
        let data_pane = Layout::default()
            .direction(Direction::Vertical)
            .constraints([Constraint::Percentage(30),Constraint::Percentage(70)].as_ref())
            .split(main_pane[0]);

        ui::draw_header(&mut f, header);

        ui::draw_logs(&mut f, main_pane[1]);
            
        Block::default()
            .borders(Borders::ALL)
            .title("Chart")
            .render(&mut f, data_pane[0]);

        ui::draw_chart(&mut f, data_pane[0], state);
        ui::draw_stats(&mut f, data_pane[1], state);

    })?;

    Ok(())
}

// Bring up enough workers to satisfy state.num_workers.
fn start_workers(state: &Arc<Mutex<AppState>>) -> Vec::<thread::JoinHandle<()>> {
    let mut r = Vec::<thread::JoinHandle<()>>::new();

    {
        let mut s = state.lock().unwrap();
        let n = s.num_threads;
        let current_workers = s.stats.len() as u8;
        if current_workers > n {
            
        }
        info!(target: "pqspam", "Starting {} worker threads for {}", n - current_workers, s.algs[0]);

        for i in current_workers..n {
            let thread_state = Arc::clone(&state);
            s.stats.push(RwLock::new(ThreadStats::default()));
            r.push(thread::spawn(move || {
                bench::do_bench(&thread_state, i);
            }));
        }

        if !s.running {
            s.start_time = timing::get_rdtsc_ticks();
        }

        s.running = true;
    }
    r
}
