use crate::AppState;
use crate::timing;

use std::sync::{Arc, Mutex};
use tui::backend::Backend;
use tui::Frame;
use tui::layout::{Rect, Constraint, Direction, Layout};
use tui::style::{Style, Modifier};
use tui::widgets::{Block, Paragraph, Text, Widget, Borders, BarChart};


pub fn draw_header<B: Backend>(mut f: &mut Frame<B>, chunk: Vec<Rect>) {
    let banner = r#"    ___  ___ ____ ___  ___ ___ _ 
   / _ \/ _ `(_-</ _ \/ _ `/  ' \
  / .__/\_, /___/ .__/\_,_/_/_/_/
 /_/     /_/   /_/               "#;

    let banner = [ Text::raw(banner) ];

    let help1 = [ Text::styled("q: Quit\nSpace: Start/Stop\nTab: Snapshot data", Style::default().modifier(Modifier::BOLD)) ];
    let help2 = [ Text::styled("r: Refresh\n+: Add thread\n-: Remove thread", Style::default().modifier(Modifier::BOLD)) ];

    Paragraph::new(banner.iter())
        .block(
            Block::default()
                .borders(Borders::NONE)
        )
        .render(&mut f, chunk[0]);
    

    Block::default()
        .borders(Borders::ALL)
        .title("Help")
        .render(&mut f, chunk[1]);

    let help_block = Layout::default()
        .direction(Direction::Horizontal)
        .margin(1)
        .constraints([Constraint::Percentage(33), Constraint::Percentage(33), Constraint::Percentage(33)].as_ref())
        .split(chunk[1]);

    Paragraph::new(help1.iter())
        .block(
            Block::default()
            .borders(Borders::NONE)
        )
        .render(&mut f, help_block[0]);
    
    Paragraph::new(help2.iter())
        .block(Block::default().borders(Borders::NONE))
    .render(&mut f, help_block[1]);
}

pub fn draw_logs<B: Backend>(mut f: &mut Frame<B>, chunk: Rect) {
    tui_logger::TuiLoggerWidget::default()
        .block(
                Block::default()
                .borders(Borders::ALL)
                .title("Logs")
        )
        .render(&mut f, chunk);
}

pub fn draw_chart<B: Backend>(mut f: &mut Frame<B>, chunk: Rect, state: &Arc<Mutex<AppState>>){
    
    let s = state.lock().unwrap();
    let points = s.samples.get_datapoints();
    drop(s);

    let latest = if points.len() > 0 {
       points[points.len()-1]
    } else {
        0.0
    };
    let mut bars = vec![];

    for p in points {
        let diff = (latest - p).abs().round();
        bars.push(("", diff as u64));
    }

    BarChart::default()
        .data(bars.as_slice())
        .max(500)
        .block(Block::default().borders(Borders::ALL).title("Absolute difference from last measurement"))
        .render(&mut f, chunk);
}

pub fn draw_stats<B: Backend>(mut f: &mut Frame<B>, chunk: Rect, state: &Arc<Mutex<AppState>>) {

    let mut stats_text: Vec<Text> = vec![];
    let mut s = state.lock().unwrap();
    if !s.running {
        Block::default()
            .borders(Borders::ALL)
            .title("Stats - Requests/sec")
            .render(&mut f, chunk);

        ()
    }
    let stats = s.calc_stats();
    let alg = s.algs[0].clone();
    let start = s.start_time;
    let tps = s.tps;
    drop(s);

    let runtime = (timing::get_rdtsc_ticks() - start) / tps;

    stats_text.push(Text::styled("[Total]: ", Style::default().modifier(Modifier::BOLD)));
    stats_text.push(Text::raw(format!("{:.3}\n", stats.total)));

    stats_text.push(Text::styled("[Mean]: ", Style::default().modifier(Modifier::BOLD)));
    stats_text.push(Text::raw(format!("{:.3}\n", stats.mean)));

    stats_text.push(Text::styled("[Std. Dev]: ", Style::default().modifier(Modifier::BOLD)));
    stats_text.push(Text::raw(format!("{:.3}\n", stats.std_dev)));

    stats_text.push(Text::raw("\n\n"));

    for i in 0 .. stats.thread_stats.len() {
        stats_text.push(Text::styled(format!("[{}]: ", i), Style::default().modifier(Modifier::BOLD)));
        stats_text.push(Text::raw(format!("{:.3} ", stats.thread_stats[i])));
    }
    
    Paragraph::new(stats_text.iter())
        .wrap(true)
        .block(
            Block::default()
            .borders(Borders::ALL)
            .title(format!("Stats - Requests/sec - {} - {}", alg, runtime ).as_str())
    ).render(&mut f, chunk);
}