extern crate openssl;

use crate::AppState;
use std::sync::{Arc, Mutex};

use openssl::ssl::{SslMethod, SslConnector, SslVerifyMode, SslSessionCacheMode};
use std::io::{Read, Write};  
use std::net::TcpStream;

// Do work for a benchmark with the given algorithm and state
pub fn do_bench(state: &Arc<Mutex<AppState>>, id: u8) {

    debug!(target: "pqspam", "Thread {} starting", id);

    // Grab the alg, host and port into a thread local variable to use later.
    let s = state.lock().unwrap();
    let alg = s.algs[0].clone();
    let host = s.host.clone();
    let port = s.port.clone();
    drop(s);

    loop{
        let conn = init_connector(&alg);
        let stream = TcpStream::connect(format!("{}:{}", host, port));

        if stream.is_err() {
            error!("[{}] TCP connect error - {:?}", id, stream);
            break;
        }
        let stream = conn.connect(&host, stream.unwrap());

        if stream.is_err() {
            error!("[{}] SSL connect error - {:?} ", id, stream);
            break;
        }

        let mut stream = stream.unwrap();
        // For now, tick steadily
        stream.write_all(b"GET / HTTP/1.0\r\n\r\n").unwrap();

        let mut res = vec![];
        stream.read_to_end(&mut res).unwrap();

        let ret = String::from_utf8(res[9..12].to_vec()).unwrap();
        // debug!(target: "pqspam", "[{}] {:?}", id, ret);
        drop(stream);

        let mut s = state.lock().unwrap();

        if ret == "200" && s.stats.len() > id as usize { // If the call was successful, increment our success counter for stats. 
            let mut stats = s.stats[id as usize].write().unwrap();
            stats.requests += 1;
            drop(stats);
        }

        if s.quit || !s.running || s.num_threads <= id || s.algs[0] != alg { //If the application is quitting or just been stopped. Or, if my thread id is not needed anymore.
            debug!(target: "pqspam", "Thread {} dying", id);
            if s.num_threads <= id && !(s.quit || !s.running || s.algs[0] != alg ) {
                s.stats.remove(id as usize);
            }
            break;
        }
        drop(s);
    }
}

fn init_connector(grp: &str) -> SslConnector {
    let mut connector = SslConnector::builder(SslMethod::tls()).unwrap();
    
    connector.set_session_cache_mode(SslSessionCacheMode::OFF);
    connector.set_verify(SslVerifyMode::NONE);
    connector.set_groups_list(grp).unwrap();
    connector.build()
} 