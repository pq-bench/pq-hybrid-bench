extern crate openssl;

use openssl::ssl::{SslMethod, SslConnector, SslVerifyMode, SslSessionCacheMode};
use std::io::{Read, Write};  
use std::net::TcpStream;

fn main() {
    
    let mut connector = SslConnector::builder(SslMethod::tls()).unwrap();
    
    connector.set_groups_list("kyber512");
    connector.set_session_cache_mode(SslSessionCacheMode::OFF);
    connector.set_verify(SslVerifyMode::NONE);

    let connector = connector.build();

    let stream = TcpStream::connect("localhost:8888").unwrap();
    
    let mut stream = connector.connect("localhost", stream).unwrap();

    let ssl_context = stream.ssl().ssl_context();

    stream.write_all(b"GET / HTTP/1.0\r\n\r\n").unwrap();
    let mut res = vec![];
    stream.read_to_end(&mut res).unwrap();
    println!("{}", String::from_utf8_lossy(&res));
}