#![feature(asm)]
extern crate core_affinity;

use core::arch::x86_64::_rdtsc;
use std::thread;
use std::time::Duration;

fn test1() { 
    let n: u8 = 10;
    for i in 0..n {
        unsafe{
            let start = _rdtsc();
            asm!("nop" :::: "volatile");
            asm!("nop" :::: "volatile");
            asm!("nop" :::: "volatile");
            asm!("nop" :::: "volatile");
            asm!("nop" :::: "volatile");
            let elapsed = _rdtsc() - start;
            println!("Pass {}: {} cycles", i, elapsed);
        }
    }
}

fn test2() {
	let mut sum: u64 = 0;
    for i in 0..5 {
        unsafe {
            let start = _rdtsc();
            thread::sleep(Duration::from_millis(1000));
            let elapsed = _rdtsc() - start;
			sum += elapsed;
            println!("Pass {}: {} cycles - {} MHz", i, elapsed, elapsed / 1000000);
        }
    }
	println!("Avg - {} - {} MHz", sum / 5, sum / 5 / 1000000);
}

fn main() {
    println!("=================");
    println!(" TSC Timer Test ");
    println!("=================\n");

    let ids = core_affinity::get_core_ids().unwrap();
    core_affinity::set_for_current(ids[0]); // Set CPU affinity

    println!("Test 1: 10 x 5 NOP, cycle count via rdtsc");
    test1();

    println!("\nTest 2: 5 x 1s thread::sleep, cycle count via rdtsc");
    test2();
}
